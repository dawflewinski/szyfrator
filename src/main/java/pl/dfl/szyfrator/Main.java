package pl.dfl.szyfrator;

import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory;
import com.sun.javafx.application.HostServicesDelegate;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {


    private static final String APPVERSION = "1.2.2";

    static String getAPPVERSION() {
        return APPVERSION;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/pl/dfl/szyfrator/views/mainView.fxml"));
        primaryStage.setTitle("Szyfrator - Cela śmierci");

        //widok
        primaryStage.setScene(new Scene(root));
        primaryStage.resizableProperty().setValue(false); //zablokowanie możliwości zmiany wielolkości okna
        primaryStage.getIcons().add(new Image("/icons/icons8-drzwi-wiezienne-z-kratami-50.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
