package pl.dfl.szyfrator.workers;

/**
 * Klasa szyfrująca wykorzystująca instrukcje wyboru switch.
 */
public class SzyfratorSwitch {

    private static SzyfratorSwitch instance = new SzyfratorSwitch();

    private SzyfratorSwitch() {
    }

    public static SzyfratorSwitch getInstance() {
        return instance;
    }

    /**
     * Metoda zwracająca ciąg liczbowy odpowiadający danemu znakowi.
     *
     * @param character przekazywany znak
     * @return wartośc odpowiadająca znakowi
     */
    private static synchronized String characterFinder(char character) {
        switch (character) {

            //literal characters
            case 'a':
            case 'ą':
                return "36";
            case 'b':
                return "35";
            case 'c':
            case 'ć':
                return "34";
            case 'd':
                return "33";
            case 'e':
            case 'ę':
                return "32";
            case 'f':
                return "31";
            case 'g':
                return "30";
            case 'h':
                return "29";
            case 'i':
                return "28";
            case 'j':
                return "27";
            case 'k':
                return "26";
            case 'l':
            case 'ł':
                return "25";
            case 'm':
                return "24";
            case 'n':
            case 'ń':
                return "23";
            case 'o':
            case 'ó':
                return "22";
            case 'p':
                return "21";
            case 'q':
                return "20";
            case 'r':
                return "19";
            case 's':
            case 'ś':
                return "18";
            case 't':
                return "17";
            case 'u':
                return "16";
            case 'v':
                return "15";
            case 'w':
                return "14";
            case 'x':
                return "13";
            case 'y':
                return "12";
            case 'z':
            case 'ż':
            case 'ź':
                return "1";

            //digit
            case '1':
                return "10";
            case '2':
                return "9";
            case '3':
                return "8";
            case '4':
                return "7";
            case '5':
                return "6";
            case '6':
                return "5";
            case '7':
                return "4";
            case '8':
                return "3";
            case '9':
                return "2";
            case '0':
                return "1";

            //white signs
            case ' ':
                return " ";
            case '\n':
                return "\n";
            case '\t':
                return "\t";

            //others -+=\/'"(){}[]:;
            case '-': return "-";
            case '+': return "+";
            case '=': return "=";
            case '\\': return "\\";
            case '/': return "/";
            case '\'': return "\'";
            case '"': return "\"";
            case '(': return "(";
            case ')': return ")";

            default:
                return "b.z.";
        }

    }


    /**
     * Metoda zamieniająca ciąg znaków na zaszyfrowany tekst.
     *
     * @param text String do tłumaczenia.
     * @return Zaszyfrowany tekst.
     */
    public static synchronized String encryptionText(String text) {

        //Przygotowanie tekstu do translacji - utrata wielkości liter ograniczająca możliwe kombinacje
        String lowerCaseText = text.toLowerCase();

        //Produkcja zaszyfrowanego tekstu
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < lowerCaseText.length(); i++) {

            if(lowerCaseText.charAt(i) == ' '){
                //dodanie spacji po spacji lub znaku nowej linii
                result.append(characterFinder(lowerCaseText.charAt(i))).append(" ");
            } else if(lowerCaseText.charAt(i) == '\n'){
                result.append(characterFinder(lowerCaseText.charAt(i)));
            }
            else {
                //dodanie nierozdzielnej spacji po znalezionym znaku
                result.append(characterFinder(lowerCaseText.charAt(i))).append("\u00A0");
            }

        }

        return result.toString();
    }
}
