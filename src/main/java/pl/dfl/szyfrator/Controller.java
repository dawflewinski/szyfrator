package pl.dfl.szyfrator;

import javafx.beans.binding.StringBinding;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import pl.dfl.szyfrator.workers.SzyfratorSwitch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Controller implements Initializable {

    /**
     * Logger
     */
    private final Logger log = Logger.getLogger(getClass().getName());

    //fixme: odwolanie do glownego okna
    private Main mainApp = new Main();

    //Constans
    private static final int MINLENGHTFIRSTTEXT = 53; //obecna długość drugiego listu
    private static final int MAXLENGHTFIRSTTEXT = 167; //obecna dlugosc pierwszego (dlugiego) listu

    private static final int MINLENGHTSECONDTEXT = 53;
    private static final int MAXLENGHTSECONDTEXT = 167;

    private static final String TOOLTIPINFOSHORT = "Tekst jest krótrzy od standarowego wykorzystywanego w pokoju. " +
            "Zalecane wydłużenie go.";
    private static final String TOOLTIPINFOOK = "Tekst jest odpowiedniej długości.";
    private static final String TOOLTIPINFOLONG = "Tekst może być za długi w tłumaczeniu. Skróć go.";

    //UI elements
    @FXML
    Button firstTextClearButton;

    @FXML
    Button secondTextClearButton;

    @FXML
    Button saveToDocButton;

    @FXML
    TextArea firstTextArea;

    @FXML
    TextArea secondTextArea;

    @FXML
    TextArea encodedFirstTextArea;

    @FXML
    TextArea encodedSecondTextArea;

    @FXML
    MenuItem aboutButton;

    @FXML
    MenuItem aboutAuthorButton;

    @FXML
    Button firstTextTips;

    @FXML
    Button secondTextTips;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //binding first textArea
        encodedFirstTextArea.textProperty().bind(new StringBinding() {
            {
                super.bind(firstTextArea.textProperty());
            }

            @Override
            protected String computeValue() {
                return encoding(firstTextArea);
            }
        });

        //binding second textArea
        encodedSecondTextArea.textProperty().bind(new StringBinding() {
            {
                super.bind(secondTextArea.textProperty());
            }

            @Override
            protected String computeValue() {
                return encoding(secondTextArea);
            }
        });

        //BINDING INFO BUTTON DLA PIERWSZEGO TEXTAREA
        firstTextTips.styleProperty().bind(new StringBinding() {

            {
                super.bind(firstTextArea.textProperty());
            }

            @Override
            protected String computeValue() {
                return setStyleBackgroundColor(firstTextArea.textProperty().getValue(), MINLENGHTFIRSTTEXT, MAXLENGHTFIRSTTEXT);
            }
        });

        //BINDING INFO BUTTON DLA drugiego TEXTAREA
        secondTextTips.styleProperty().bind(new StringBinding() {

            {
                super.bind(secondTextArea.textProperty());
            }

            @Override
            protected String computeValue() {
                return setStyleBackgroundColor(secondTextArea.textProperty().getValue(), MINLENGHTSECONDTEXT, MAXLENGHTSECONDTEXT);
            }
        });

        //bindowanie tekstu pierwszego tootipa informacyjnego
        firstTextTips.getTooltip().textProperty().bind(new StringBinding() {
            {
                super.bind(firstTextArea.textProperty());
            }

            @Override
            protected String computeValue() {
                return textForTooltipInfo(firstTextArea.textProperty().getValue(), MINLENGHTFIRSTTEXT, MAXLENGHTFIRSTTEXT);
            }
        });

        //bindowanie tekstu drugiego tootipa informacyjnego
        secondTextTips.getTooltip().textProperty().bind(new StringBinding() {
            {
                super.bind(secondTextArea.textProperty());
            }

            @Override
            protected String computeValue() {
                return textForTooltipInfo(secondTextArea.textProperty().getValue(), MINLENGHTSECONDTEXT, MAXLENGHTSECONDTEXT);
            }
        });

    }

    private String encoding(TextArea textArea) {

        return SzyfratorSwitch.encryptionText(textArea.textProperty().getValue());
    }

    //methods to encoding text
    @FXML
    private void encodingFirstText() {
        encodedFirstTextArea.setText(SzyfratorSwitch.encryptionText(firstTextArea.getText()));
    }

    @FXML
    private void encodingSecondText() {
        encodedSecondTextArea.setText(SzyfratorSwitch.encryptionText(secondTextArea.getText()));
    }

    @FXML
    private void clearFirstText() {
        firstTextArea.setText("");

        //niepotrzebne - zastosowano bindowanie
//        encodedFirstTextArea.setText("");
    }

    @FXML
    private void clearSecondText() {
        secondTextArea.setText("");

        //niepotrzebne - zastosowanie bindowanie
//        encodedSecondTextArea.setText("");
    }


    //O programie popup
    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Szyfrator " + Main.getAPPVERSION());
        alert.setHeaderText(null);
        VBox contentVBox = new VBox();

        Hyperlink enigmatHyperlink = new Hyperlink("www.enigmatescape.pl");
        enigmatHyperlink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                mainApp.getHostServices().showDocument("http://www.enigmatescape.pl");
            }
        });

        contentVBox.getChildren().addAll(
                labelWithWrapperedText("Program wytworzony na potrzeby firmy Enigmat Escape Rooms."),
                enigmatHyperlink,
                labelWithWrapperedText("Zamienia przekazywany tekst na zaszyfrowany. Wykorzystywany jest w celu " +
                        "szybkiego tworzenia materiału do zagadki " +
                        "polegającej na odkodowaniu zaszyfrowanej wiadomości w pokoju ucieczki - Cela Śmierci.")
        );

        alert.getDialogPane().setContent(contentVBox);
        alert.getDialogPane().setMaxWidth(400);
        alert.showAndWait();
    }

    //O autorze popup
    @FXML
    private void aboutAuthor(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);//Create message box with information only.
        alert.setTitle("Autor");
        alert.setHeaderText(null);
        alert.setContentText("Dawid F. Lewiński \ndaw.lewinski@gmail.com \n\nEnigmat Escape Rooms");
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);//Workaround for wrapping text.
        alert.showAndWait();//Blocking dialog waits for user reaction.
    }

    //Metoda do generowania pliku docx z przetłumaczonym tekstem
    @FXML
    private void saveToDocx(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Zapis do pliku");
        fileChooser.setInitialFileName("Nowy plik");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Plik tekstowy(*.docx)", "*.docx"));
        File file = fileChooser.showSaveDialog(((Node) event.getTarget()).getScene().getWindow());
        if (file != null) {

            //utworzenie dokumentu
            XWPFDocument document = new XWPFDocument();

            //paragraf - tekst pierwszy
            addParagraph(document, "Treść pierwszego tekstu:");
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, splitTextToArrayList(firstTextArea.getText()), 10);
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, "");

            //paragraf - tekst pierwszy przetlumaczony
            addParagraph(document, "Zakodowany pierwszy tekst:");
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, splitTextToArrayList(encodedFirstTextArea.getText()), 12);
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, "");

            //linia pozioma
            document.createParagraph().setBorderBottom(Borders.SINGLE);

            //paragraf - tekst drugi
            addParagraph(document, "Treść drugiego tekstu:");
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, splitTextToArrayList(secondTextArea.getText()), 10);
            addParagraph(document, "");
            addParagraph(document, "");

            //paragraf - tekst drugi przetlumaczony
            addParagraph(document, "Zakodowany drugi tekst:");
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, splitTextToArrayList(encodedSecondTextArea.getText()), 12);
            addParagraph(document, "");
            addParagraph(document, "");
            addParagraph(document, "");

            //linia pozioma
            document.createParagraph().setBorderBottom(Borders.SINGLE);

            try {
                FileOutputStream out = new FileOutputStream(file);
                document.write(out);
                out.close();
                System.out.println(file.toString() + " written successully");

            } catch (IOException e) {
                e.printStackTrace();
                log.log(Level.WARNING, e.getMessage());
            }
        }
    }

    private void addParagraph(XWPFDocument document, String paragraphText) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setFontSize(12);
        run.setText(paragraphText);

    }

    private void addParagraph(XWPFDocument document, String paragraphText, int fontSize) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setFontSize(fontSize);
        run.setText(paragraphText);
    }

    private ArrayList<String> splitTextToArrayList(String text) {

        ArrayList<String> resultList = new ArrayList<String>();

        StringBuilder singleParagraph = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '\n') {
                resultList.add(singleParagraph.toString());
                singleParagraph = new StringBuilder();
            } else {
                singleParagraph.append(text.charAt(i));
            }
        }
        resultList.add(singleParagraph.toString());
        return resultList;
    }

    /**
     * Dodanie paragrafów do pliku docx
     *
     * @param document      dokument w którym ma zostać dodana lista dokumentów
     * @param paragraphText lista paragrafów
     * @param fontSize      rozmiar fontu w paragrafie
     */
    private void addParagraph(XWPFDocument document, ArrayList<String> paragraphText, int fontSize) {

        for (String aParagraphText : paragraphText) {
            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();
            run.setFontSize(fontSize);
            run.setText(aParagraphText);
        }
    }

    private int countCharToTranslate(String text) {
        //fixme metoda zliczajaca znaki do tlumaczenia - nieliczenie bialych znakow itp
        int result = 0;

        result = text.length();

        return result;
    }

    //metoda zmianiajaca kolor buttona w zaleznosci od przekazywanych parametrow
    private void setBackgroundColor(String text, Node infoNode, int minLenght, int maxLenght) {
        int lettersCounter = countCharToTranslate(text);

        if (lettersCounter < minLenght) {
            infoNode.setStyle("-fx-background-color: yellow");
            //dodanie tooltipa z komentarzem
        } else if (lettersCounter < maxLenght) {
            infoNode.setStyle("-fx-background-color: green");
            //dodanie tooltipa z komentarzem
        } else {
            infoNode.setStyle("-fx-background-color: red");
            //dodanie tooltipa z komentarzem
        }
    }

    //metoda zwracajaca text styli (kolor tla) w zaleznosci od dlugosci tekstu
    private String setStyleBackgroundColor(String text, int minLenght, int maxLenght) {
        int lettersCounter = countCharToTranslate(text);

        if (lettersCounter < minLenght) {
            return "-fx-background-color: yellow; -fx-opacity: 0.3;";

        } else if (lettersCounter < maxLenght) {
            return "-fx-background-color: green; -fx-opacity: 0.3;";

        } else {
            return "-fx-background-color: red; -fx-opacity: 0.3;";
        }
    }

    //metoda zwracajaca tekst od tooltipa w zaleznosci od dlugosci tekstu
    private String textForTooltipInfo(String text, int minLenght, int maxLenght) {
        int lettersCounter = countCharToTranslate(text);

        if (lettersCounter < minLenght) {
            return TOOLTIPINFOSHORT;

        } else if (lettersCounter < maxLenght) {
            return TOOLTIPINFOOK;

        } else {
            return TOOLTIPINFOLONG;
        }

    }

    //metoda zwracajaca label z wraptextem
    private Label labelWithWrapperedText(String text) {
        Label label = new Label(text);
        label.setWrapText(true);
        label.setMaxWidth(Parent.BASELINE_OFFSET_SAME_AS_HEIGHT);

        return label;
    }

}
